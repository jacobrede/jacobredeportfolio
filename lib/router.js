
Router.configure({
  layoutTemplate: 'appLayout'
});

Router.route('/', function () {
  this.render('navigation', {to: 'header'});
  this.render('homePage', {to: 'content'});
  this.render('footer', {to: 'footer'});
});
