if (Meteor.isClient) {

  Template.homePage.events({
    'click #cryptography' : function() {
      Modal.show('cryptography');
    },
    'click #imageManipulation' : function() {
      Modal.show('imageManipulation');
    },
    'click #mysql' : function() {
      Modal.show('mysql');
    }

  });
}
